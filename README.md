# iflytekDemo

#### 介绍
继承并重新封装了科大讯飞语音听写, 语音合成, 语音评测三个功能, 配置了腾讯的tinker热更新, 开发框架主要集成了RxPermissions动态权限申请, RxJava2异步任务框架, 腾讯QMUI组件, 腾讯arch库, butterKnife注入,retrofit网络请求库, gson解析, Aria文件下载框架, glide图片加载框架, 以及两个开源的组件库(加载动画和dialog组件).
项目遵循谷歌提倡的MVVM架构, 结合JetPack搭建项目.
项目目前仍保留了原生asynctask异步线程任务的写法.


#### 软件架构
暂无


#### 安装教程

1.  后端服务需自行更改
2.  科大讯飞SDK需自行前往科大讯飞官网进行下载并替换文件

#### 使用说明

暂无

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

暂无
